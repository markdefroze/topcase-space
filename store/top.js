export const state = () => ({
  ordered: []
})

export const getters = {
  ordered: (state) => state.ordered
}

export const mutations = {
  SET_ORDERED(state, payload) {
    state.ordered = payload
  }
}

export const actions = {
  async fetchOrdered({commit}) {
    let {data} = await this.$axios.get('v1/shop/top/ordered')
    commit('SET_ORDERED', data.data)
  }
}
