import unionBy from 'lodash.unionby'

export const state = () => ({
  from_name: null,
  from_email: null,
  from_phone: null,
  delivery_zipcode: null,
  delivery_address: null,
  note: null,
  details_attributes: []
})

export const getters = {
  details: (state, getters, rootState, rootGetters) => {
    return state.details_attributes.map(item => {
      item.product = rootGetters['database/productById'](item.shop_product_id)
      return item
    })
  },
  count: (state) => state.details_attributes.length,
  findById: (state) => (id) => state.details_attributes.find(item => item.shop_product_id === Number(id)),
  totalAmount: (state, getters) => {
    let totalAmount = 0;

    getters.details.map(item => {
      if(item.product)
      totalAmount += item.quantity * item.product.product_price
    })

    return totalAmount
  }
}

export const mutations = {
  ADD(state, payload) {
    state.details_attributes.push({shop_product_id: payload.id, quantity: 1})
  },
  REMOVE(state, payload) {
    state.details_attributes = state.details_attributes.filter(item => item.shop_product_id !== payload.id);
  },
  UPDATE_QUANTITY(state, {product, value}) {
    const detail = state.details_attributes.find(item => item.shop_product_id === product.id)
    detail.quantity = value
    state.details_attributes = unionBy(state.details_attributes, [detail], 'shop_product_id')
  },
  UPDATE_ORDER(state, {key, value}) {
    state[key] = value
  },
  RESET(state) {
    state.details_attributes = []
  }
}

export const actions = {
  add({commit, rootState}, payload) {
    commit('ADD', payload)
    commit('database/UPDATE_PRODUCTS', [payload], { root: true})
    UIkit.notification(rootState.i18n.messages.cart.add.notification, 'primary')
  },
  async checkout({state, commit, rootState})  {
    let response = await this.$axios.post('v1/shop/orders', state)
    if(response.status === 201) {
      this.$router.push({ name: 'orders-slug', slug: response.data.id })
      UIkit.notification(rootState.i18n.messages.cart.send.notification, 'primary')
      commit('RESET')
    }
  }
}

