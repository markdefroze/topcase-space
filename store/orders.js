export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
  async getOne(context, { slug }) {
    let { data } = await this.$axios.get(`v1/shop/orders/${slug}`)

    return data
  },
}

