export const state = () => ({
  product: null,
  canvas: null,
  colors: ['#1abc9c', '#2ecc71', '#3498db','#9b59b6','#f1c40f','#e67e22','#e74c3c'],
  fonts: ['Times New Roman', 'Lobster','Pacifico','Pattaya','Philosopher','Poiret One']
})

export const getters = {
  canvas: (state) => state.canvas,
  fonts: (state) => state.fonts,
  colors: (state) => state.colors,
  getObjects: (state) => {
    if(state.canvas) {
      return state.canvas.getObjects()
    }
  },
  getActiveObject: (state) => {
    if(state.canvas) {
      return state.canvas.getActiveObject()
    }
  },
}

export const mutations = {
  INIT(state, {canvas}) {
    canvas.controlsAboveOverlay = true;
    state.canvas = canvas
  },
  SET_PRODUCT(state, {product}) {
    state.product = product
  },
  SETUP(state) {
    if(state.product.properties) {
      state.canvas.setBackgroundImage(state.product.properties.background, state.canvas.renderAll.bind(state.canvas), {
        crossOrigin: 'anonymous'
      })
      state.canvas.setOverlayImage(state.product.properties.overlay, state.canvas.renderAll.bind(state.canvas), {
        crossOrigin: 'anonymous'
      })
    }
  },
  SET_ACTIVE_OBJECT(state, {object}) {
    state.canvas.setActiveObject(object)
    state.canvas.renderAll()
  },
  CLEAR(state) {
    if(state.canvas.getObjects()) {
      state.canvas.getObjects().map(object => state.canvas.remove(object))
      state.canvas.renderAll()
    }
  },
  REMOVE_OBJECT(state) {
    if(state.canvas.getActiveObject()) {
      state.canvas.remove(state.canvas.getActiveObject())
      state.canvas.renderAll()
    }
  },
  BRING_FORWARD(state) {
    if(state.canvas.getActiveObject()) {
      state.canvas.getActiveObject().bringForward()
      state.canvas.renderAll()
    }
  },
  SEND_BACKWARDS(state){
    if(state.canvas.getActiveObject()) {
      state.canvas.getActiveObject().sendBackwards()
      state.canvas.renderAll()
    }
  },
  ADD_TEXT(state) {
    let textbox = new fabric.Textbox('topcase.space')
    textbox.set({
      cornerColor: 'black',
      borderColor: 'black',
    });
    state.canvas.add(textbox).setActiveObject(textbox)
    textbox.center()
  },
  CHANGE_BACKGROUND_COLOR(state, {value}) {
    state.canvas.backgroundImage = null
    state.canvas.setBackgroundColor(value)
    state.canvas.renderAll()
  },
  CHANGE_PROPERTY_OBJECT(state, {key, value}) {
    if(state.canvas.getActiveObject()) {
      state.canvas.getActiveObject().set(key, value)
      state.canvas.renderAll()
    }
  },
  ADD_IMAGE(state, {e}) {
    let reader = new FileReader()
    reader.onload = e => {
      let file = reader.result

      fabric.Image.fromURL(file, (img) => {
        img.scaleToWidth(250);
        // img.scaleToHeight(600);
        state.canvas.add(img).renderAll()
        img.center();
        img.set({
          cornerColor: 'black',
          borderColor: 'black',
        });
        state.canvas.setActiveObject(img);
      })

    }
    reader.readAsDataURL(e.target.files[0])
    e.target.value = null
  }
}

export const actions = {
  init({commit}, {product}) {
    let canvas = new fabric.Canvas('constructor')

    commit('INIT', {canvas})
    commit('SET_PRODUCT', {product})
    commit('SETUP')
  },
  clear({commit}) {
    commit('CLEAR')
    commit('SETUP')
  },
  download({state}) {
    let link = document.createElement("a");
    link.href = state.canvas.toDataURL()
    link.download = "topcase-space.png"
    link.click()
  },
  async upload({state, dispatch}, payload = {}) {
    let {status, data: { data }} = await this.$axios.post('v1/shop/products', Object.assign(state.product, {
      api_key: process.env.api_key,
      title: state.product.title,
      constructor: 'custom',
      shop_category_id: state.product.category.id,
      image: state.canvas.toDataURL(),
      properties_attributes: [
        {
          key: 'background',
          value: state.product.properties.background,
        },
        {
          key: 'overlay',
          value: state.product.properties.overlay,
        },
      ]
    }))

    if(status === 201 && payload.redirect) {
      dispatch('cart/add', data, {root: true})
      this.$router.push({ name: 'checkout'})
      UIkit.notification('Custom case created and add your cart.', 'primary')
    } else {
      this.$router.push({ name: 'index'})
    }
  }
}

