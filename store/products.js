export const state = () => ({
  results: [],
  items: [],
  currentPage: 0,
  totalPages: 0,
  count: 0,
})

export const getters = {
  items: (state) => state.items,
  searchResults: (state) => state.results,
  countResults: (state) => state.results.length,
  count: (state) => state.count,
  nextPage: (state) => state.currentPage + 1,
  hasNext: (state) => state.totalPages > state.currentPage,
  prevPage: (state) => state.currentPage - 1,
  hasPrev: (state) => state.currentPage > 1
}

export const mutations = {
  SET_ALL(state, payload) {
    state.items = payload.data
    if(payload.page) {
      state.currentPage = payload.page.current_page
      state.totalPages = payload.page.total_pages
    }
    state.count = payload.count
  },
  SET_RESULTS(state, payload) {
    state.results = payload.data
  },
  RESET_RESULTS(state) {
    state.results = []
  },
}

export const actions = {
  async fetchAll({state, commit}, {search, category, constructorValue, page, tag, limit }) {
    let per_page = limit || process.env.products.per_page
    let {data} = await this.$axios.get('v1/shop/products', { params: { constructor: constructorValue, tag, search, category, page, per_page } })
    commit('SET_ALL', data)

    return { totalCount: data.count }
  },
  async search({state, commit}, {search}) {
    if(search.length >= 3) {
      let {data} = await this.$axios.get('v1/shop/products', { params: { search } })
      commit('SET_RESULTS', data)
    } else {
      commit('RESET_RESULTS')
    }
  }
}

