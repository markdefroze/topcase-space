export const state = () => ({
  items: [],
  currentPage: 0,
  totalPages: 0,
  count: 0,
})

export const getters = {
  items: (state) => state.items,
  count: (state) => state.count,
  nextPage: (state) => state.currentPage + 1,
  hasNext: (state) => state.totalPages > state.currentPage,
  prevPage: (state) => state.currentPage - 1,
  hasPrev: (state) => state.currentPage > 1
}

export const mutations = {
  SET_ALL(state, payload) {
    state.items = payload.data
    if(payload.page) {
      state.currentPage = payload.page.current_page
      state.totalPages = payload.page.total_pages
    }
    state.count = payload.count
  },
}

export const actions = {
  async fetchPosts({state, commit}, {page}) {
    let {data} = await this.$axios.get('v1/blog/posts', { params: { page, per_page: process.env.posts.per_page } })
    commit('SET_ALL', data)

    return { totalCount: data.count }
  },
}

