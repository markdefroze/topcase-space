import unionBy from 'lodash.unionby'
import orderBy from 'lodash.orderby'
import {version as versionApp} from "../package";

export const state = () => ({
  version: '',
  tags: [],
  categories: [],
  products: [],
})

export const getters = {
  categories: (state) => state.categories,
  tags: (state) => orderBy(state.tags, 'title', 'asc'),
  products: (state) => state.products,
  productById: (state) => (id) => state.products.find(item => item.slug === id || item.id === Number(id))
}

export const mutations = {
  SET_VERSION(state, payload) {
    state.version = payload.version
  },
  UPDATE_TAGS(state, payload) {
    state.tags = unionBy(state.tags, payload, 'id')
  },
  UPDATE_PRODUCTS(state, payload) {
    state.products = unionBy(state.products, payload, 'id')
  },
  UPDATE_CATEGORIES(state, payload) {
    state.categories = unionBy(state.categories, payload, 'id')
  }
}

export const actions = {
  async Init( { state, commit, dispatch, rootState } ) {
    let version;

    try {
      version = localStorage.getItem('version')
    } catch (e) {}

    if(versionApp !== version)
    {
      localStorage.setItem('version', versionApp)

      let localStorageEnabled = false;
      try {
        localStorageEnabled = !!localStorage;
      } catch(e) {}

      if (localStorageEnabled) {
        console.log('DATABASE:clear')
        localStorage.removeItem('store')
      }

    }
  },
  async fetchTags({state, commit}) {
    let {data} = await this.$axios.get('v1/shop/tags')
    commit('UPDATE_TAGS', data)
  },
  async fetchProduct({state, commit}, {id}) {
    let {data} = await this.$axios.get('v1/shop/products/' + id)
    commit('UPDATE_PRODUCTS', data)
  },
  async fetchCategories({state, commit}) {
    let {data} = await this.$axios.get('v1/shop/categories')
    commit('UPDATE_CATEGORIES', data)
  }
}
