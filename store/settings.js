export const state = () => ({
  contact: null,
  app: null,
})

export const getters = {
  app: (state) => state.app,
  contact: (state) => state.contact,
}

export const mutations = {
  SET_ALL(state, { contact, app }) {
    state.contact = contact
    state.app = app
  },
}

export const actions = {
  async fetchAll({state, commit}) {
    let {data} = await this.$axios.get('v1/site/settings')
    commit('SET_ALL', data)

    return { totalCount: data.count }
  },
}

