export const state = () => ({
})

export const getters = {
}

export const mutations = {
}

export const actions = {
  async getOne(context, { slug }) {
    let { data } = await this.$axios.get(`v1/site/pages/${slug}`)

    return data
  },
  async sendMail({ rootState }, { model }) {
    let { data: { message } } = await this.$axios.post(`v1/site/mails`, { ...model, api_key: process.env.api_key })

    if(message === 'RecordCreate') {
      UIkit.notification(rootState.i18n.messages.contact.notification, 'primary')
      this.$router.push({ name: 'index' })
    }
  }
}

