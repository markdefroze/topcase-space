import { version } from '../package'

export const strict = false

export const actions = {
  async nuxtServerInit( { state, commit, dispatch } ) {
    await dispatch('settings/fetchAll')
  },
}
