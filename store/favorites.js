export const state = () => ({
  items: []
})

export const getters = {
  items: (state, getters, rootState, rootGetters) => {
    return state.items.map(id => {
      let product = rootGetters['database/productById'](id)
      return { shop_product_id: id, product }
    })
  },
  findById: (state) => (id) => state.items.find(item => item === Number(id))
}

export const mutations = {
  LIKE(state, payload) {
    if(state.items.indexOf(payload.id) === -1) {
      state.items.push(payload.id)
    } else {
      state.items = state.items.filter(item => item !== payload.id)
    }
  },
}

export const actions = {
  like({commit}, payload) {
    commit('LIKE', payload)
    commit('database/UPDATE_PRODUCTS', [payload], { root: true})
  }
}

