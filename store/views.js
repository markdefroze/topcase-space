export const state = () => ({
  items: []
})

export const getters = {
  items: (state, getters, rootState, rootGetters) => {
    return state.items.map(id => {
      let product = rootGetters['database/productById'](id)
      return { shop_product_id: id, product }
    }).reverse()
  },
}

export const mutations = {
  UPDATE(state, payload) {
    state.items = state.items.filter(item => item !== payload.id).slice(0,2)
    state.items.push(payload.id)
  }
}


export const actions = {
  update({commit}, payload) {
    commit('UPDATE', payload)
    commit('database/UPDATE_PRODUCTS', [payload], { root: true})
  }
}
