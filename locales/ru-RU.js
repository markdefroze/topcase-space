export default {
  statuses: {
    accepted: 'Принят',
    processed: 'Обрабатывается',
    pending: 'Ожидает оплаты',
    payed: 'Оплачен',
    shipped: 'Доставляется',
    delivered: 'Доставлен по адрессу',
    waiting: 'Ожидает получения',
    completed: 'Завершен',
  },
  contact: {
    notification: 'Письмо успешно отправленно. Ожидайте ответа на вашу почту'
  },
  previous: 'Предыдущая',
  next: 'Следующая',
  brand: {
    name: 'TopCase.space'
  },
  cart: {
    table: {
      product: 'Товар',
      price: 'Цена',
      quantity: 'Количество',
      total: 'Всего',
      remove: 'Удалить',
    },
    title: 'Корзина',
    button: {
      remove: 'Удалить из корзины',
      add: 'Добавить в корзину',
    },
    add: {
      notification: 'Товар добавлен в корзину',
    },
    send: {
      notification: 'Спасибо, ваш заказ обрабатывается',
    },
    total: 'Всего',
    checkout: {
      title: 'Оформить заказ',
      button: 'Корзина',
      information: 'Основная информация',
      form: {
        fullName: {
          placeholder: 'Полное имя'
        },
        email: {
          placeholder: 'Электронная почта'
        },
        phone: {
          placeholder: 'Мобильный телефон'
        },
        address: {
          placeholder: 'Адрес доставки (город, улица, дом, квартира)'
        },
        zipCode: {
          placeholder: 'Индекс'
        },
        notes: {
          placeholder: 'Пожелания к заказу'
        },
        button: 'Оформить заказ'
      },
      order: {
        title: 'Ваш заказ'
      }
    }
  },
  favorites: {
    title: 'Избранные товары'
  },
  constructor: {
    index: {
      title: 'Выберите модель',
      button: {
        customize: 'Выбрать'
      }
    },
    layer: 'Слой',
    title: 'Создать свой дизайн',
    button: {
      customize: 'Создать свой дизайн товара в конструкторе',
    },
    selectAnother: 'Выбрать другой',
    addText: 'Добавить текст',
    addImage: 'Добавить картинку',
    download: 'Скачать',
    upload: 'Добавить в корзину',
    save: 'Сохранить',
    clear: 'Сбросить все',
    background: 'Фон',
    backgroundColor: 'Фоновый цвет',
    opacity: 'Порозрачность',
    fontFamily: 'Шрифт',
    fontSize: 'Размер шрифта',
    color: 'Цвет',
    text: {
      label: 'Текст',
      placeholder: 'Введите текст ...',
    }
  },
  navbar: {
    home: 'Главная',
    products: 'Продукты',
    about: 'О нас',
    contacts: 'Контакты',
  },
  products: {
    content: 'Описание',
    messages: {
      notFound: 'Извините товары не найдены. Если не нашли нужный товар, Вы можете создать свой дизайн товара самостоятельно в <a href="/constructor">конструкторе товаров</a>.'
    },
    recently: {
      title: 'Недавно просмотренные товары'
    },
    ordered: {
      title: 'Самые популярные товары'
    }
  },
  search: {
    title: 'Результаты поиска',
    placeholder: 'Поиск товара ...'
  },
  blog: {
    latest: {
      title: 'Последние новости',
      more: {
        button: 'Читать дальше'
      },
      subtitle: 'Новости, акции, конкурсы и другая полезная информация',
    }
  }
}
