export default {
  previous: 'Previous',
  next: 'Next',
  brand: {
    name: 'TopCase.space'
  },
  cart: {
    table: {
      product: 'Product',
      price: 'Price',
      quantity: 'Quantity',
      total: 'Total',
      remove: 'Remove',
    },
    title: 'Cart',
    button: {
      remove: 'Remove from cart',
      add: 'Add to cart',
    },
    add: {
      notification: 'Product add to cart',
    },
    send: {
      notification: 'Thank you, your order is being processed',
    },
    total: 'Total',
    checkout: {
      title: 'Checkout',
      button: 'Check out',
      information: 'General information',
      form: {
        fullName: {
          placeholder: 'Full name'
        },
        email: {
          placeholder: 'Email'
        },
        phone: {
          placeholder: 'Phone'
        },
        address: {
          placeholder: 'Address'
        },
        zipCode: {
          placeholder: 'ZIP code'
        },
        notes: {
          placeholder: 'Notes'
        },
        button: 'Checkout'
      },
      order: {
        title: 'Your order'
      }
    }
  },
  favorites: {
    title: 'Favorites'
  },
  constructor: {
    layer: 'Layer',
    title: 'Customize',
    button: {
      customize: 'Customize product in constructor',
    },
    selectAnother: 'Select another product',
    addText: 'Add Text',
    addImage: 'Add Image',
    download: 'Download',
    upload: 'Upload',
    clear: 'Clear',
    background: 'Background',
    backgroundColor: 'Background Color',
    opacity: 'Opacity',
    fontFamily: 'Font Family',
    fontSize: 'Font Size',
    color: 'Color',
    text: {
      label: 'Text',
      placeholder: 'Some text ...',
    }
  },
  navbar: {
    home: 'Home',
    products: 'Products',
    about: 'About us',
    contacts: 'Contacts',
  },
  products: {
    content: 'Content',
    messages: {
      notFound: 'Products not found'
    },
    recently: {
      title: 'Recently viewed products'
    },
    ordered: {
      title: 'Most ordered products'
    }
  },
  search: {
    title: 'Search results',
    placeholder: 'Search here your product...'
  },
  blog: {
    latest: {
      title: 'Latest blog',
      more: {
        button: 'Learn more'
      },
      subtitle: 'There are many variations of passages of brands available,',
    }
  }
}
