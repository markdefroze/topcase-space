module.exports = {
  env: {
    api_key: 'dfac02a814e1178e876f8f2cad58d93b',
    payeer: {
      key: 'mBxoLwXM7MZnRvfI',
      shop_id: '625786463',
    },
    products: {
      per_page: 8
    },
    posts: {
      per_page: 4
    }
  },
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - topcase.space',
    title: 'Крутые чехлы на iPhone, Samsung, Xiaomi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Интернет-магазин прикольных чехлов на заказ topcase.space - купить чехол и заказать печать на чехлах по доступным ценам.' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.5/css/uikit.min.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Roboto:400,500,700|Lobster|Pacifico|Pattaya|Philosopher|Poiret+One' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.2.3/fabric.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.5/js/uikit.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.5/js/uikit-icons.min.js' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/router',
    // '@nuxtjs/pwa',
    '@nuxtjs/browserconfig',
    // '@nuxtjs/google-analytics',
    ['nuxt-validate', {
      lang: 'ru',
    }],
    ['nuxt-i18n', {
      seo: false,
      // vuex: false,
      locales: [
        {
          code: 'ru',
          file: 'ru-RU.js'
        },
      ],
      lazy: true,
      langDir: 'locales/',
      defaultLocale: 'ru',
      vueI18n: {
        fallbackLocale: 'ru',
      }
    }]
  ],
  // workbox: {
  //   runtimeCaching: [
  //     {
  //       urlPattern: 'http://topcase.space/.*',
  //       strategyOptions: {
  //         cacheName: 'our-cache',
  //         cacheExpiration: {
  //           maxEntries: 10,
  //           maxAgeSeconds: 300
  //         }
  //       }
  //     }
  //   ]
  // },
  meta: {
    theme_color: '#1e87f0',
  },
  browserconfig: {
    tile: {
      TileColor: '#1e87f0',
      square70x70logo: {'@':{src:'ms-icon-70x70.png'}},
      square150x150logo: {'@':{src:'ms-icon-150x150.png'}},
      square310x310logo: {'@':{src:'ms-icon-310x310.png'}}
    }
  },
  manifest: {
    name: 'topcase.space',
    lang: 'ru'
  },
  // 'google-analytics': {
  //   id: 'UA-15299585-1'
  // },
  router: {
    linkActiveClass: 'uk-active',
    linkExactActiveClass: 'uk-active',
  },
  axios: {
    proxy: true, // Can be also an object with default options
    prefix: '/api/',
    retry: { retries: 3 }
  },
  proxy: {
    '/api': 'http://store.topcase.space',
  },
  plugins: [
    { src: '~plugins/socialSharing', ssr: false },
    { src: '~plugins/axios', ssr: true },
    { src: '~plugins/filters', ssr: true },
    { src: '~plugins/localStorage', ssr: false }
  ],
  loading: { color: '#1e87f0' },
  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
