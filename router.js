import Vue from 'vue'
import Router from 'vue-router'

import IndexPage from '~/pages/index'
import CheckoutPage from '~/pages/checkout'
import CartPage from '~/pages/cart'
import WishlistPage from '~/pages/wishlist'
import FavoritesPage from '~/pages/favorites'
import ProductPage from '~/pages/products/_slug'
import ConstructorIndexPage from '~/pages/constructor'
import ProductsPage from '~/pages/products'
import SearchPage from '~/pages/search'
import CatalogPage from '~/pages/catalog'
import TagPage from '~/pages/tag'
import PagesOnePage from '~/pages/pages/_slug'
import ConstructorPage from '~/pages/constructor/_slug'
import ContactPage from '~/pages/contact'
import OrderPage from '~/pages/orders/_slug'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'index',
    component: IndexPage
  },
  {
    path: '/cart',
    name: 'cart',
    component: CartPage
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: CheckoutPage
  },
  {
    path: '/wishlist',
    name: 'wishlist',
    component: WishlistPage
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: FavoritesPage
  },
  {
    path: '/constructor',
    name: 'constructor',
    component: ConstructorIndexPage,
    props: true
  },
  {
    path: '/constructor/page/:page(\\d+)',
    name: 'constructor-page',
    component: ConstructorIndexPage,
    props: true
  },
  {
    path: '/constructor/:slug',
    name: 'constructor-slug',
    component: ConstructorPage,
    props: true
  },
  {
    path: '/products',
    name: 'products',
    component: ProductsPage,
    props: true
  },
  {
    path: '/products/page/:page(\\d+)',
    name: 'products-page',
    component: ProductsPage,
    props: true
  },
  {
    path: '/products/:slug',
    name: 'products-slug',
    component: ProductPage,
    props: true
  },
  {
    path: '/search/:search',
    props: true,
    component: SearchPage,
    children: [
      {
        path: '',
        name: 'search',
        component: SearchPage,
      },
      {
        path: 'page/:page(\\d+)',
        name: 'search-page',
        component: SearchPage,
      }
    ]
  },
  {
    path: '/catalog/:category',
    props: true,
    component: CatalogPage,
    children: [
      {
        path: '',
        name: 'catalog',
        component: CatalogPage,
        props: true
      },
      {
        path: 'page/:page(\\d+)',
        name: 'catalog-page',
        component: CatalogPage,
        props: true
      }
    ]
  },
  {
    path: '/tag/:tag',
    props: true,
    component: TagPage,
    children: [
      {
        path: '',
        name: 'tag',
        component: TagPage,
        props: true
      },
      {
        path: 'page/:page(\\d+)',
        name: 'tag-page',
        component: TagPage,
        props: true
      }
    ]
  },
  {
    path: '/pages/:slug',
    name: 'pages-page',
    component: PagesOnePage,
    props: true,
  },
  {
    path: '/contact',
    name: 'contact',
    component: ContactPage,
    props: true,
  },
  {
    path: '/order/:slug',
    name: 'orders-slug',
    component: OrderPage,
    props: true,
  },
]

export function createRouter() {
  return new Router({
    mode: 'history',
    routes,
    scrollBehavior: (to, from, savedPosition) => {
      return { x: 0, y: 0 }
    }
  })
}
