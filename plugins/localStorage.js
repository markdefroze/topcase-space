import createMutationsSharer from 'vuex-shared-mutations'
import VuexPersistence from 'vuex-persist'

export default ({ store, isHMR }) => {
  if (isHMR) return;
  if (process.client) {

    (new VuexPersistence({
      key: 'store',
      modules: ['database', 'cart', 'favorites', 'views'],
    })).plugin(store)

    createMutationsSharer({ predicate: ['database/UPDATE_PRODUCTS','cart/ADD', 'cart/REMOVE', 'cart/UPDATE_QUANTITY', 'cart/UPDATE_ORDER', 'favorites/LIKE', 'views/UPDATE'] })(store)
  }
}
