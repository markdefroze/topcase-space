import Vue from 'vue'
import SocialSharing from 'vue-social-sharing'

export default ({ store, isHMR }) => {
  Vue.use(SocialSharing)
}
